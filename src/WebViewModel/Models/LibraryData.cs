﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebViewModel.Models
{

    public class LibraryData
    {
        public List<Book> Books { get; set; }

        public LibraryData()
        {
            Books = new List<Book>
            {
                new Book {  Id = 1,
                            ISBN ="123456789",
                            Title ="Learning C#" ,
                            numberOfPages = 420,
                            Author = new Author {
                                            Id = 1,
                                            FirstName = "James",
                                            LastName = "Sharp",
                                            BirthDate = new DateTime(1980,5,20)
                                            }
                            },
                new Book {  Id = 2,
                            ISBN ="45689132",
                            Title ="Mastering Linq" ,
                            numberOfPages = 360,
                            Author = new Author {
                                            Id = 2,
                                            FirstName = "Linqy",
                                            LastName = "Winqy",
                                            BirthDate = new DateTime(1992,3,4)
                                            }
                            },
                new Book {  Id = 3,
                            ISBN ="789456258",
                            Title ="Cooking is fun" ,
                            numberOfPages = 40,
                            Author = new Author {
                                            Id = 3,
                                            FirstName = "Elisa",
                                            LastName = "Yammy",
                                            BirthDate = new DateTime(1996,8,12)
                                            }
                            }
            };
        }

        public Book GetBookById(int id)
        {
            return Books.FirstOrDefault(b => b.Id == id);
        }
        public List<Book> GetAllBooks()
        {
            return Books;
        }
    }
}

